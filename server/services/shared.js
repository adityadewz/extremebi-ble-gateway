module.exports = {
    GetDevice: () => {
        return axios.get(App.serverUrl + '/devices/uuid/' + App.DEVICE_ID);
    },
    CalculateDistance: (rssi, txPower = -65) => {
        var txPower = txPower //hard coded power value. Usually ranges between -59 to -65
        if (rssi == 0) {
            return -1.0;
        }
        var ratio = (txPower - rssi) / 20;
        var distance = Math.pow(10, ratio)
        return distance;
    },
}