var packages = require('./global.packages');

Object.keys(packages).forEach(k => {
    if (!global[k]) {
        global[k] = require(packages[k]);
    }
})

global.App = require('./global.config.js')();