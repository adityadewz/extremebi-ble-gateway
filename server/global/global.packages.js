module.exports = {
    _: 'underscore',
    moment: 'moment',
    fs: 'fs',
    path: 'path',
    axios: 'axios',
    noble: 'noble'
}