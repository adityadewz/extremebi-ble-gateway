var mongoDBUrl;
var env = process.env.NODE_ENV;
var port;
var fs = require('fs');
var path = require('path');
var appUrl;
var serverUrl;
var cache = {};
var sslOptions = {};

var DEVICE_ID = process.env.BLE_DEVICE_ID;

switch (env) {
    case 'production':
        appUrl = 'https://dashboard.extremebi.io';
        serverUrl = 'https://api.extremebi.io';
        port = 443;
        break;

    case 'development':
        appUrl = 'http://development.extremebi.io';
        serverUrl = 'http://192.168.0.2:3800';
        port = 80;
        break;

    default:
        appUrl = 'http://localhost:4200';
        serverUrl = 'http://localhost:3800';
        port = 3800;
        break;
}

module.exports = () => {
    return {
        port: port,
        DEVICE_ID: DEVICE_ID,
        UPLOAD_INTERVAL: process.env.UPLOAD_INTERVAL,
        appUrl: appUrl,
        SCAN_INTERVAL: 5000,
        serverUrl: serverUrl,
        env: env,
        sslOptions: sslOptions,
        timeZone: 'Asia/Calcutta',
        images: {
            logo: 'https://s3-us-west-2.amazonaws.com/assets.extremebi.io/extremebi/logo-with-text.png',
            pushLogo: 'https://s3-us-west-2.amazonaws.com/assets.extremebi.io/extremebi/push.png'
        }
    }
}