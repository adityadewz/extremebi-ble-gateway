require('dotenv').config()
var express = require("express")
var app = express()
var path = require("path")
var bodyParser = require("body-parser")
var routes = require("./routes")
var io = require("socket.io-client")
var compression = require('compression')
var bootstrap = require('./bootstrap');

app.use(express.static(path.join(__dirname, '../static')));
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());
app.use(compression());
bootstrap();

Service.Shared.GetDevice().then(d => {
    var DEVICE = d.data.device;
    var BEACONS = d.data.beacons;
    // console.log(DEVICE, 'DEVICE')
    App.DEVICE = DEVICE;
    App.BEACONS = BEACONS;
    if (App.DEVICE) {
        var socket = io(App.serverUrl, {
            query: {
                type: 'device',
                device: DEVICE._id,
                organisation: DEVICE.organisation
            },
        });
        global.App.socket = socket;
        routes(socket);
    }

}).catch(er => {
    console.log(er, 'error')
})

app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!')
});