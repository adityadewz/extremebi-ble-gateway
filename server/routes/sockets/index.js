module.exports = function(socket) {
    socket.on('connect', () => {
        console.log('Connected to Bakend!');
        var BEACONS = App.BEACONS;
        noble.on('stateChange', function(state) {
            if (state === 'poweredOn') {
                noble.startScanning([], true);
            } else {
                noble.stopScanning();
            }
        });
        noble.on('discover', function(peripheral) {
            BEACONS.forEach(bea => {
                if (bea.uuid == peripheral.id) {
                    console.log('Discovered: ' + peripheral);
                    console.log(`The distance art which the device is ${Service.Shared.CalculateDistance(peripheral.rssi)}`);
                    var packet = {
                        data: {
                            rssi: peripheral.rssi,
                            uuid: peripheral.id
                        },
                        device: App.DEVICE._id,
                        date: moment().toISOString()
                    }
                    socket.emit('BLE_DEVICE_DATA_TO_SERVER', packet);
                }
            });
        });
    });

    socket.on('SCAN_BEACONS_ON_GATEWAY', (payload) => {
        var BEACONS = App.BEACONS;
        noble.stopScanning();
        noble.startScanning();
        noble.on('discover', function(peripheral) {
            BEACONS.forEach(bea => {
                if (bea.uuid == peripheral.id) {
                    console.log('Discovered: ' + peripheral);
                    console.log(`The distance art which the device is ${Service.Shared.CalculateDistance(peripheral.rssi)}`);
                    socket.emit('BLE_DEVICE_DATA_TO_SERVER', {
                        data: {
                            rssi: peripheral.rssi,
                            uuid: peripheral.id,
                            txPowerLevel: peripheral.advertisement.txPowerLevel
                        },
                        device: App.DEVICE._id,
                        date: payload.date
                    });
                }
            });
        });
    });


    socket.on('disconnect', () => {
        console.log('Disconnecting!');
    })
}