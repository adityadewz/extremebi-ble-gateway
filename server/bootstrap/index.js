var moment = require('moment-timezone');
var toTitleCase = require('to-title-case');
var fs = require('fs');
var path = require('path')
const SERVICES = 'services'
global.Service = {};
global.App = {};

moment.tz.setDefault('Asia/Calcutta');

module.exports = function(app) {
    require('../global')
    var serviceFiles = fs.readdirSync(path.join(__dirname, '../' + SERVICES));
    serviceFiles.forEach(file => {
        var name = toTitleCase(file.split('.')[0]).split(' ').join('')
        global.Service[name] = require('../' + SERVICES + '/' + file);
    })
}